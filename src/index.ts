import { IRenderMime } from '@jupyterlab/rendermime-interfaces';
import { Widget } from '@phosphor/widgets';
import { Message} from '@phosphor/messaging';
import '../style/index.css';


const MIME_TYPE = 'application/x-root';
const CLASS_NAME = 'mimerenderer-root';


export class JSROOTWidget extends Widget implements IRenderMime.IRenderer {

  constructor(options: IRenderMime.IRendererOptions) {
    super({ node: Private.createNode() });

    this.addClass(CLASS_NAME);
    // @jupyterlab/apputils causes build problems,
    // have to copy @jupyterlab/apputils/IFrame widget implementation
    // instead of inheriting from it :(
    this.addClass('jp-IFrame');
  }

  renderModel(model: IRenderMime.IMimeModel): Promise<void> {
    if (this._objectUrl) {
      return Promise.resolve(void 0);
    }

    let data = model.data[MIME_TYPE] as string;
    // If there is no data, do nothing.
    if (!data) {
      return Promise.resolve(void 0);
    }
    const blob = Private.b64toBlob(data, MIME_TYPE);

    this._objectUrl = URL.createObjectURL(blob);

    this.node.querySelector('iframe')!.setAttribute('srcdoc', `
      <script src="https://root.cern.ch/js/5.6.1/scripts/JSRootCore.min.js?gui">
      </script>
      <style>
        .jsroot_browser_title,
        .jsroot_browser_version,
        .h_tree > div:nth-child(1) > div:nth-child(1)
        { display: none; }
      </style>
      <div id="simpleGUI" file="${this._objectUrl}" noselect></div>
    `);

    return Promise.resolve(void 0);
  }

  onBeforeDetach(msg: Message): void {
    const iframe = this.node.querySelector('iframe')!;
    (iframe.contentWindow! as any).JSROOT.hpainter.Cleanup();
  }

  dispose() {
    try {
      URL.revokeObjectURL(this._objectUrl);
    } catch (error) {
      /* no-op */
    }
    super.dispose();
  }

  private _objectUrl = '';
}

export const rendererFactory: IRenderMime.IRendererFactory = {
  safe: true,
  mimeTypes: [MIME_TYPE],
  createRenderer: options => new JSROOTWidget(options)
};

const extension: IRenderMime.IExtension = {
  id: 'jupyterlab_jsroot:plugin',
  rendererFactory,
  rank: 0,
  dataType: 'string',
  fileTypes: [
    {
      name: 'JSROOT',
      displayName: 'JSROOT',
      fileFormat: 'base64',
      mimeTypes: [MIME_TYPE],
      extensions: ['.root']
    }
  ],
  documentWidgetFactoryOptions: {
    name: 'JSROOT',
    modelName: 'base64',
    primaryFileType: 'JSROOT',
    fileTypes: ['JSROOT'],
    defaultFor: ['JSROOT']
  }
};

export default extension;

namespace Private {
  /**
   * Create the main content node of an iframe widget.
   */
  export function createNode(): HTMLElement {
    let node = document.createElement('div');
    let iframe = document.createElement('iframe');
    iframe.style.height = '100%';
    iframe.style.width = '100%';
    node.appendChild(iframe);
    return node;
  }
  /**
   * Convert a base64 encoded string to a Blob object.
   * Modified from a snippet found here:
   * https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
   *
   * @param b64Data - The base64 encoded data.
   *
   * @param contentType - The mime type of the data.
   *
   * @param sliceSize - The size to chunk the data into for processing.
   *
   * @returns a Blob for the data.
   */
  export function b64toBlob(
    b64Data: string,
    contentType: string = '',
    sliceSize: number = 3072
  ): Blob {
    const byteCharacters = atob(b64Data);
    let byteArrays: Uint8Array[] = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);

      let byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      let byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, { type: contentType });
  }
}

