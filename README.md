# jupyterlab_jsroot

A JupyterLab extension for rendering root files.

## Prerequisites

* JupyterLab

## Installation

```bash
git clone https://gitlab.cern.ch/spbu-jupyterhub/jupyterlab_jsroot.git
jupyter labextension install .
```

## Development

For a development install (requires npm version 4 or later), do the following in the repository directory:

```bash
npm install
jupyter labextension link .
```

To rebuild the package and the JupyterLab app:

```bash
npm run build
jupyter lab build
```

